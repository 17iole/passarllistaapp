package com.example.eloi.appllista;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eloi on 20/03/2016.
 */
public class DataBaseHelper extends SQLiteOpenHelper{
    public static final String DATABASE_NAME ="estudiants.db";
    public static final String TABLE_NAME ="taula_grups";
    public static final String COL_1 ="ID";
    public static final String COL_2 ="NOM_GRUP";
    public static final String COL_3 ="NUMERO_ALUMNES";

    public static final String EST_TABLE_NAME ="taula_alumnes";
    public static final String EST_COL_1 ="ID";
    public static final String EST_COL_2 ="NOM_ALUMNE";
    public static final String EST_COL_3 ="COGNOMS_ALUMNE";
    public static final String EST_COL_4 ="EDAT";
    public static final String EST_COL_5 ="GRUP";
    public static final String EST_COL_6 ="NOTES";



    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 2);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, NOM_GRUP TEXT, NUMERO_ALUMNES INTEGER)");
        db.execSQL("CREATE TABLE IF NOT EXISTS " + EST_TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, NOM_ALUMNE TEXT, COGNOMS_ALUMNE TEXT, EDAT INTEGER, GRUP INTEGER, NOTES TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + EST_TABLE_NAME);
        onCreate(db);
    }

    public boolean crearNouGrup(String nom){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2, nom);
        contentValues.put(COL_3, "0");
        long result = db.insert(TABLE_NAME, null, contentValues);
        long numGrups = DatabaseUtils.queryNumEntries(db, TABLE_NAME);


        db.execSQL("CREATE TABLE IF NOT EXISTS ASSISTENCIA_" + Long.toString(numGrups) + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, DIA DATETIME DEFAULT CURRENT_TIMESTAMP, ALUMNES_QUE_HAN_VINGUT INTEGER)");
        Log.d("execSQL", "nom:" + nom);

        if(result==-1)
            return false;
        else
            return true;
    }
    SQLiteDatabase db = this.getWritableDatabase();

    public boolean crearNouAlumne(String nom, String cognom, String edat, String grup, String notes){

        ContentValues contentValues = new ContentValues();
        contentValues.put(EST_COL_2, nom);
        contentValues.put(EST_COL_3, cognom);
        contentValues.put(EST_COL_4, edat);
        contentValues.put(EST_COL_5, grup);
        contentValues.put(EST_COL_6, notes);
        long result = db.insert(EST_TABLE_NAME, null, contentValues);
        if(result==-1)
           return false;
        else{
            long alumnesDelGrup = DatabaseUtils.longForQuery(db, "SELECT COUNT(ID) from taula_alumnes where GRUP = " + grup, null);
            db.execSQL("UPDATE taula_grups SET NUMERO_ALUMNES=" + alumnesDelGrup + " where ID = " + grup);

            long idalumne = DatabaseUtils.longForQuery(db, "SELECT COUNT(ID) from taula_alumnes", null);

            db.execSQL("ALTER TABLE ASSISTENCIA_"+grup+" ADD COLUMN '"+idalumne+"' INTEGER");
            return true;
        }

    }
    public long totalAlumnes (String grup) {
        long total_alumnes = DatabaseUtils.longForQuery(db, "SELECT NUMERO_ALUMNES FROM taula_grups WHERE ID=" + grup, null);
        return total_alumnes;
    }

    public Cursor llistaDies(String grup){
        SQLiteDatabase db = this.getWritableDatabase();
        Log.d("rawQuery", "SELECT ID _id,* FROM ASSISTENCIA_"+grup);
        Cursor c = db.rawQuery("SELECT ID _id,* FROM ASSISTENCIA_"+grup, null);
        return c;
    }

    long id_llista=0;
    public void passarLlista(Integer i, Integer total_alumnes, String grup, String alumne, Integer check){
        if(i==0){
            id_llista = DatabaseUtils.longForQuery(db, "SELECT COUNT(ID) from ASSISTENCIA_" + grup, null);
            db.execSQL("INSERT INTO ASSISTENCIA_" + grup +" ('" +alumne + "') VALUES ("+check+")");
        }else {
            db.execSQL("UPDATE ASSISTENCIA_" + grup +" SET ALUMNES_QUE_HAN_VINGUT= "+ total_alumnes +", '"+alumne+"' = "+check+" WHERE ID="+(id_llista+1));
            Log.d("SQLITE", "UPDATE ASSISTENCIA_" + grup +" SET ALUMNES_QUE_HAN_VINGUT= "+ total_alumnes +", '"+alumne+"' = "+check+" WHERE ID="+(id_llista+1));
        }

    }

    public Cursor getAllRows(String TABLA){

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT ID _id,* FROM "+ TABLA, null);
        return c;
    }
    public Cursor getAlumnesGrup(String GRUP){

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT ID _id,* FROM "+EST_TABLE_NAME+" WHERE GRUP =\'" + GRUP + "\'", null);
        return c;
    }
    public Cursor getAlumne(String ID){

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT taula_alumnes.ID _id,* FROM "+EST_TABLE_NAME+", "+ TABLE_NAME+" WHERE taula_alumnes.id="+ID+" AND taula_grups.id =taula_alumnes.GRUP" , null);
        return c;
    }

    /*public Cursor passarLlista(String Id_grup){

    }*/

    public List<String> getAllGrups(){
        List<String> labels = new ArrayList<String>();

        // Select All Query
        String selectQuery = "SELECT " +COL_1+","+ COL_2+ " FROM "+TABLE_NAME;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if(cursor!=null && cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    labels.add(cursor.getString(0)+" "+cursor.getString(1));
                } while (cursor.moveToNext());
            }
        }

            Log.d("SPINNER",labels.toString());

        // closing connection
        cursor.close();
        db.close();

        // returning lables
        return labels;
    }

}
