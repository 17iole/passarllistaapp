package com.example.eloi.appllista;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class DetallsAlumne extends AppCompatActivity {
    public static String alumne="";
    public static String Id_alumne="";
    DataBaseHelper myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalls_alumne);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("Detalls de l'alumne");
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fab.hide();

        ImageView image = (ImageView) findViewById(R.id.fotoAlumne);

        Intent intent = getIntent();
        String name = intent.getStringExtra("NomAlumne");
        String Id = intent.getStringExtra("Id");
        if(name!=null){
            alumne=name;
            Id_alumne=Id;
        }
        detallsAlumne(Id_alumne);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_alumne, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        Log.d("TAG", "Has clicat a afegir alumne ______________________________________________");
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if(id==R.id.editAlumne){
            startActivity(new Intent(this, EditAlumne.class));
        }
        return super.onOptionsItemSelected(item);
    }

    private void detallsAlumne(String Id_alumne) {

        myDb = new DataBaseHelper(this);
        Cursor cursor = myDb.getAlumne(Id_alumne);
        String[] from = new String[]{DataBaseHelper.EST_COL_2, DataBaseHelper.EST_COL_3, DataBaseHelper.COL_2, DataBaseHelper.EST_COL_5, DataBaseHelper.EST_COL_6};
        int[] to = new int[]{R.id.fitxa_nom, R.id.fitxa_cognoms,R.id.fitxa_grup, R.id.fitxa_edat, R.id.fitxa_notes};
        final SimpleCursorAdapter myCursorAdapter = new SimpleCursorAdapter(getApplicationContext(), R.layout.detalls_alumne_dins, cursor, from, to);
        final ListView myList = (ListView) findViewById(R.id.llista_detalls_alumne);
        myList.setAdapter(myCursorAdapter);

        /*myList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                          @Override
                                          public void onItemClick (AdapterView < ? > adapter, View view,int position, long arg){
                                              // TODO Auto-generated method stub
                                              TextView v = (TextView) view.findViewById(R.id.text_element);
                                              TextView d = (TextView) view.findViewById(R.id.id_llista);
                                              Intent i = new Intent(view.getContext(), DetallsAlumne.class);  // get a valid context
                                              i.putExtra("NomAlumne", v.getText());  //I don't know where sign came from
                                              i.putExtra("Id", d.getText());
                                              startActivity(i);
                                          }
                                      }
        );*/

    }

}
