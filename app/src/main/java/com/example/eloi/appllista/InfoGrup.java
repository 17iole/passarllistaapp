package com.example.eloi.appllista;


import android.app.ActionBar;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.Bundle;
import android.support.percent.PercentLayoutHelper;
import android.support.percent.PercentRelativeLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class InfoGrup extends Fragment {
    public InfoGrup() {
        // Required empty public constructor
    }
    DataBaseHelper myDb;
    private View myFragmentView;
    private TextView text_total_alumnes;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        myFragmentView = inflater.inflate(R.layout.fragment_info_grup, container, false);
        text_total_alumnes = (TextView)myFragmentView.findViewById(R.id.total_alumnes);

        DataBaseHelper bd= new DataBaseHelper(this.getContext());

        long total_alumnes=bd.totalAlumnes(LlistaGrup.Id_grup);
//in your OnCreate() method
        text_total_alumnes.setText(Long.toString(total_alumnes));
        populateListView();
        //estilLlista();
        return myFragmentView;
    }
    /*
    void estilLlista(){
        Log.d("ESTil", "yeah");
        ListView millista = (ListView) myFragmentView.findViewById(R.id.llista_dies_assistencia);
        for (int i = 0; i < (millista.getCount()); i++) {
            Log.d("for", Integer.toString(i));
            TextView numero = (TextView) getViewByPosition(i,millista).findViewById(R.id.num_alumnes_dia);
            View linia_percentatge =getViewByPosition(i,millista).findViewById(R.id.linia_percentatge);

            Log.d("num_alumnes_dia", numero.getText().toString());
            linia_percentatge.setLayoutParams(new android.view.ViewGroup.LayoutParams(200, 200));
        }
    }

    public View getViewByPosition(int pos, ListView listView) {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition ) {
            return listView.getAdapter().getView(pos, null, listView);
        } else {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }*/

    private void populateListView() {
        myDb = new DataBaseHelper(getActivity());
        Cursor cursor = myDb.llistaDies(LlistaGrup.Id_grup.toString());
        String[] from = new String[]{"ID", "DIA", "ALUMNES_QUE_HAN_VINGUT"};
        int[] to = new int[]{R.id.id_llista,R.id.data,R.id.num_alumnes_dia};
        final SimpleCursorAdapter myCursorAdapter = new SimpleCursorAdapter(getActivity(), R.layout.element_llista_dia_assistencia, cursor, from, to);
        final ListView myList = (ListView) myFragmentView.findViewById(R.id.llista_dies_assistencia);
        myList.setAdapter(myCursorAdapter);
        /*
        myList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                  @Override
                  public void onItemClick(AdapterView<?> adapter, View view, int position, long arg) {
                      // TODO Auto-generated method stub
                      TextView v = (TextView) view.findViewById(R.id.data);
                      TextView d = (TextView) view.findViewById(R.id.id_llista);
                      Intent i = new Intent(view.getContext(), LlistaGrup.class);  // get a valid context
                      i.putExtra("NomGrup", v.getText());  //I don't know where sign came from
                      i.putExtra("Id", d.getText());
                      startActivity(i);
                  }
              }
        );*/
    }

}
