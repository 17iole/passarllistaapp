package com.example.eloi.appllista;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class LlistaAlumnesGrup extends Fragment {
    DataBaseHelper myDb;
    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;

    public LlistaAlumnesGrup() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_llista_alumnes_grup, container, false);
    }

    private void populateListView() {
        myDb = new DataBaseHelper(getActivity());
        Cursor cursor = myDb.getAlumnesGrup(LlistaGrup.Id_grup);
        String[] from = new String[]{DataBaseHelper.EST_COL_1, DataBaseHelper.EST_COL_2, DataBaseHelper.EST_COL_3};
        int[] to = new int[]{R.id.id_llista, R.id.data, R.id.cognom_alumne};
        final SimpleCursorAdapter myCursorAdapter = new SimpleCursorAdapter(getActivity(), R.layout.element_llista_alumne, cursor, from, to);
        final ListView myList = (ListView) getView().findViewById(R.id.llista_alumnes_grup);
        myList.setAdapter(myCursorAdapter);

        myList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                          @Override
                                          public void onItemClick (AdapterView < ? > adapter, View view,int position, long arg){
                                              // TODO Auto-generated method stub
                                              TextView v = (TextView) view.findViewById(R.id.data);
                                              TextView d = (TextView) view.findViewById(R.id.id_llista);
                                              Intent i = new Intent(view.getContext(), DetallsAlumne.class);  // get a valid context
                                              i.putExtra("NomAlumne", v.getText());  //I don't know where sign came from
                                              i.putExtra("Id", d.getText());
                                              startActivity(i);
                                          }
                                      }
        );
    }
    @Override
    public void onResume(){
        super.onResume();
        populateListView();
    }

}
