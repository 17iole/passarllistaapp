package com.example.eloi.appllista;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class LlistaGrup extends AppCompatActivity {
    public static String grup_seleccionat="";
    public static String Id_grup="";
    DataBaseHelper myDb;
    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_llista_grup);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        tabLayout =(TabLayout)findViewById(R.id.tabLayout);
        viewPager =(ViewPager)findViewById(R.id.viewPager);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragments(new LlistaAlumnesGrup(), "Llista Alumnes");
        viewPagerAdapter.addFragments(new InfoGrup(), "Informació");
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Intent i = new Intent(view.getContext(), PassarLlista.class);  // get a valid context
                i.putExtra("NomGrup", grup_seleccionat);  //I don't know where sign came from
                i.putExtra("Id", Id_grup);
                startActivity(i);
            }
        });
        fab.setImageDrawable(getDrawable(R.mipmap.check));
        Intent intent = getIntent();
        String name = intent.getStringExtra("NomGrup");
        String Id = intent.getStringExtra("Id");
        if(name!=null){
            grup_seleccionat=name;
            Id_grup=Id;
        }
        getSupportActionBar().setTitle(grup_seleccionat);
        //populateListView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        Log.d("TAG", "Has clicat a afegir alumne ______________________________________________");
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id==R.id.nouAlumne){
            startActivity(new Intent(this, NouAlumne.class));
            Intent n = new Intent(getApplicationContext(), NouAlumne.class);  // get a valid context
            n.putExtra("NomGrup", grup_seleccionat);  //I don't know where sign came from
            n.putExtra("Id", Id_grup);
            startActivity(n);
        }
        return super.onOptionsItemSelected(item);
    }

    /*private void populateListView() {
        myDb = new DataBaseHelper(this);
        Cursor cursor = myDb.getAlumnesGrup(Id_grup);
        String[] from = new String[]{DataBaseHelper.EST_COL_1, DataBaseHelper.EST_COL_2, DataBaseHelper.EST_COL_3};
        int[] to = new int[]{R.id.id_llista, R.id.nom_alumne, R.id.cognom_alumne};
        final SimpleCursorAdapter myCursorAdapter = new SimpleCursorAdapter(getApplicationContext(), R.layout.element_llista_alumne, cursor, from, to);
        final ListView myList = (ListView) findViewById(R.id.llista_alumnes_grup);
        myList.setAdapter(myCursorAdapter);

        myList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
              @Override
              public void onItemClick (AdapterView < ? > adapter, View view,int position, long arg){
                  // TODO Auto-generated method stub
                  TextView v = (TextView) view.findViewById(R.id.nom_alumne);
                  TextView d = (TextView) view.findViewById(R.id.id_llista);
                  Intent i = new Intent(view.getContext(), DetallsAlumne.class);  // get a valid context
                  i.putExtra("NomAlumne", v.getText());  //I don't know where sign came from
                  i.putExtra("Id", d.getText());
                  startActivity(i);
              }
          }
        );
    }
    @Override
    public void onResume(){
        super.onResume();
        populateListView();
    }*/
}
