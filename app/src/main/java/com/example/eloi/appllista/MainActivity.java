package com.example.eloi.appllista;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    DataBaseHelper myDb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        myDb = new DataBaseHelper(this);
        populateListView();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setImageDrawable(getDrawable(R.mipmap.nougrup));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                openNewGroupActivity(view);
            }
        });
    }

    public void openNewGroupActivity(View view) {
        startActivity(new Intent(this, NouGrup.class));
    }

    private void populateListView() {
        Cursor cursor = myDb.getAllRows("taula_grups");
        String[] from = new String[]{DataBaseHelper.COL_1, DataBaseHelper.COL_2, DataBaseHelper.COL_3};
        int[] to = new int[]{R.id.id_llista,R.id.data,R.id.num_alumnes_dia};
        final SimpleCursorAdapter myCursorAdapter = new SimpleCursorAdapter(getApplicationContext(), R.layout.element_llista_1, cursor, from, to);
        final ListView myList = (ListView) findViewById(R.id.llistagrups);
        myList.setAdapter(myCursorAdapter);
        myList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick (AdapterView < ? > adapter, View view,int position, long arg){
                // TODO Auto-generated method stub
                TextView v = (TextView) view.findViewById(R.id.data);
                TextView d = (TextView) view.findViewById(R.id.id_llista);
                Intent i = new Intent(view.getContext(), LlistaGrup.class);  // get a valid context
                i.putExtra("NomGrup", v.getText());  //I don't know where sign came from
                i.putExtra("Id", d.getText());
                startActivity(i);
            }
        }
        );
    }

    @Override
    public void onResume(){
        super.onResume();
        populateListView();

    }
}