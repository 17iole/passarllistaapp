package com.example.eloi.appllista;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.List;

public class NouAlumne extends AppCompatActivity {
    DataBaseHelper myDb;
    Button btn_afegirAlumne;
    EditText et_nomA, et_cognomA,  et_notesA;
    Spinner spinnerEdat, spinnerGrups;
    public static String Id_grup="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nou_alumne);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        Id_grup = intent.getStringExtra("Id");
        loadSpinnerData();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fab.hide();

        spinnerEdat=(Spinner)findViewById(R.id.spinner_edatA);
        ArrayAdapter adapter=ArrayAdapter.createFromResource(this, R.array.edats, android.R.layout.simple_dropdown_item_1line);
        spinnerEdat.setAdapter(adapter);
        crearAlumne();

    }

    public void pickImage(View view){
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);

    }

    /**
     * Function to load the spinner data from SQLite database
     * */
    private void loadSpinnerData() {
        DataBaseHelper myDb;
        // database handler
        myDb = new DataBaseHelper(this);
        spinnerGrups=(Spinner)findViewById(R.id.spinner_grupA);

        // Spinner Drop down elements
        List<String> lables = myDb.getAllGrups();

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(NouAlumne.this, android.R.layout.simple_spinner_item, lables);
        Log.d("lables", "lables---------------->" + lables.toString());

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        // attaching data adapter to spinner
        //Spinner spinnerEdatA = (Spinner) findViewById(R.id.spinner_edatA);
        spinnerGrups.setAdapter(dataAdapter);
        spinnerGrups.setSelection((Integer.parseInt(Id_grup)-1));
    }

    public void crearAlumne(){
        myDb = new DataBaseHelper(this);
        btn_afegirAlumne = (Button)findViewById(R.id.btn_guardarnouA);
        et_nomA = (EditText)findViewById(R.id.et_nomA);
        et_notesA =(EditText)findViewById(R.id.et_notesA);
        et_cognomA =(EditText)findViewById(R.id.et_cognomA);
        btn_afegirAlumne.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String g = "";
                        g =spinnerGrups.getSelectedItem().toString().substring(0,1);
                        Log.d("TAG", "he clicat ----------------" + myDb.toString());
                        boolean isInsterted = myDb.crearNouAlumne(et_nomA.getText().toString(), et_cognomA.getText().toString(), spinnerEdat.getSelectedItem().toString(), g, et_notesA.getText().toString());
                        if (isInsterted) {
                            Log.d("TAG", "data inserted // context: " + getApplicationContext().toString());
                            Toast.makeText(NouAlumne.this, "Alumne creat", Toast.LENGTH_SHORT).show();
                        } else {
                            Log.d("TAG", "data not inserted");
                            Toast.makeText(NouAlumne.this, "Error, alumne no creat", Toast.LENGTH_SHORT).show();}
                    }
                }
        );
    }
}
