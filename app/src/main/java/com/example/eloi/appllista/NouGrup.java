package com.example.eloi.appllista;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NouGrup extends AppCompatActivity {

    EditText edit_nom;
    Button btn_afegirgrup;
    DataBaseHelper myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("TAG", "onCreate NouGrup");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nougrup);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.hide();

        myDb = new DataBaseHelper(this);
        btn_afegirgrup = (Button)findViewById(R.id.btn_afegirgrup);
        edit_nom = (EditText)findViewById(R.id.editText_nomgrup);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        crearGrup();
    }

    public void crearGrup () {

        Log.d("TAG", "ha entrat al crearGrup() ----------------");
        myDb = new DataBaseHelper(this);
        btn_afegirgrup = (Button)findViewById(R.id.btn_afegirgrup);
        edit_nom = (EditText)findViewById(R.id.editText_nomgrup);
        btn_afegirgrup.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("TAG", "he clicat ----------------"+myDb.toString());
                        boolean isInsterted = myDb.crearNouGrup(edit_nom.getText().toString());
                        if (isInsterted) {
                            Log.d("TAG", "data inserted // context: "+ getApplicationContext().toString());
                            Toast.makeText(NouGrup.this, "Grup creat", Toast.LENGTH_SHORT).show();
                            finish();
                        }else{
                            Log.d("TAG", "data not inserted");
                            Toast.makeText(NouGrup.this, "Error al crear el grup", Toast.LENGTH_SHORT).show();}
                    }
                }
        );
    }

}
