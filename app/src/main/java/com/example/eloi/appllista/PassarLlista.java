package com.example.eloi.appllista;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eloi.appllista.dummy.myCursorAdapter;

import java.util.Calendar;

public class PassarLlista extends AppCompatActivity {
    public static String grup_seleccionat="";
    public static String Id_grup="";
    DataBaseHelper myDb;
    Button btn_guardarLlista;
    TextView id_alumne;
    CheckBox check;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_passar_llista);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fab.hide();
        Intent intent = getIntent();
        String name = intent.getStringExtra("NomGrup");
        String Id = intent.getStringExtra("Id");
        if(name!=null){
            grup_seleccionat=name;
            Id_grup=Id;
        }
        getSupportActionBar().setTitle("Passar llista "+grup_seleccionat);
        TextView text_data = (TextView) findViewById(R.id.data_llista);

        final Calendar c = Calendar.getInstance();
        int yy = c.get(Calendar.YEAR);
        int mm = c.get(Calendar.MONTH);
        int dd = c.get(Calendar.DAY_OF_MONTH);

        // set current date into textview
        text_data.setText(new StringBuilder()
                // Month is 0 based, just add 1
                .append(dd).append(" / ").append(mm + 1).append(" / ").append(yy));
        View footerView = getLayoutInflater().inflate(R.layout.button_footer, null);
        ListView llista = (ListView)findViewById(R.id.llista_alumnes_passarllista);
        populateListView();
        //addFooterView(footerView);
        llista.addFooterView(footerView);
        btn_guardarLlista = (Button)findViewById(R.id.button_guardar_llista);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if (id == R.id.action_settings) {return true;}
        if(id==R.id.nouAlumne){startActivity(new Intent(this, NouAlumne.class));}
        return super.onOptionsItemSelected(item);
    }

    private void populateListView() {
        myDb = new DataBaseHelper(this);
        Log.d("ID_GRUP: ------>>>",Id_grup.toString());
        Cursor cursor = myDb.getAlumnesGrup(Id_grup);
        String[] from = new String[]{DataBaseHelper.EST_COL_1, DataBaseHelper.EST_COL_2, DataBaseHelper.EST_COL_3};
        int[] to = new int[]{R.id.id_llista, R.id.data, R.id.cognom_alumne};
        final myCursorAdapter adapter = new myCursorAdapter(getApplicationContext(), R.layout.element_llista_passarllista, cursor, from, to);
        final ListView llista = (ListView) findViewById(R.id.llista_alumnes_passarllista);
        llista.setAdapter(adapter);

    }

    public void guardarLlista(View view){
        Toast.makeText(this,"Guardant llista...", Toast.LENGTH_LONG).show();
        int total_alumnes=0;
        ListView llista = (ListView) findViewById(R.id.llista_alumnes_passarllista);
        for (int i = 0; i < (llista.getCount()-1); i++) {
            TextView textalumne =(TextView)getViewByPosition(i,llista).findViewById(R.id.id_llista);

            String alumne=textalumne.getText().toString();
            Log.d("id: ", alumne);
            int checkstate = myCursorAdapter.checkBoxState[i] ? 1 : 0;
            total_alumnes=total_alumnes+checkstate;
            Log.d("total_alumnes",Integer.toString(total_alumnes));
            myDb.passarLlista(i, total_alumnes, Id_grup, alumne, checkstate);
        }
    }

    public View getViewByPosition(int pos, ListView listView) {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition ) {
            return listView.getAdapter().getView(pos, null, listView);
        } else {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }

}