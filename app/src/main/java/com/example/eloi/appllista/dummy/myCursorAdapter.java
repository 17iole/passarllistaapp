package com.example.eloi.appllista.dummy;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;


import com.example.eloi.appllista.DataBaseHelper;
import com.example.eloi.appllista.R;

import java.util.Arrays;

public class myCursorAdapter extends SimpleCursorAdapter {

    private LayoutInflater layoutInflater;
    private int layout;
    private int nameColIndex;
    private int cognomColIndex;
    private int idColIndex;
    private int priorityColIndex;
    public static boolean[] checkBoxState;

    public myCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
        super(context, layout, c, from, to);
        Log.d("EIII", "HEENTRAT al myCursorAdapter!!!--------------------------->");
        this.layout = layout;
        layoutInflater = LayoutInflater.from(context);
        nameColIndex = c.getColumnIndex(DataBaseHelper.EST_COL_2);
        cognomColIndex = c.getColumnIndex(DataBaseHelper.EST_COL_3);
        idColIndex = c.getColumnIndex(DataBaseHelper.EST_COL_1);
        checkBoxState = new boolean[c.getCount()];
        Log.d("EIII", "---> c.getCount()"+Integer.toString(c.getCount()));
        //priorityColIndex =c.getColumnIndex(DataBaseHelper.KEY_PRIORITY);
    }

    @Override
    public View newView(Context context, final Cursor cursor, ViewGroup parent) {

        View view = layoutInflater.inflate(layout, parent, false);

        TextView titleText = (TextView) view.findViewById(R.id.data);
        TextView cognomText = (TextView) view.findViewById(R.id.cognom_alumne);
        TextView idText = (TextView) view.findViewById(R.id.id_llista);
        CheckBox checkboxText =(CheckBox) view.findViewById(R.id.checkBox_passarllista);
        //ImageView priorityTag = (ImageView) view.findViewById(R.id.item_priority);

        NoteHolder holder = new NoteHolder();
        holder.nameView = titleText;
        holder.cognomView = cognomText;
        holder.idView = idText;
        holder.checkbox = checkboxText;
        holder.checkbox.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Log.d("checkBoxState",checkBoxState.toString());
                if (((CheckBox) v).isChecked())
                    checkBoxState[cursor.getPosition()] = true;
                else
                    checkBoxState[cursor.getPosition()] = false;

            }
        });
        holder.checkbox.setChecked(checkBoxState[cursor.getPosition()]);
        //holder.priorityView = priorityTag;
        holder.title = cursor.getString(nameColIndex);
        holder.cognom = cursor.getString(cognomColIndex);
        holder.id = cursor.getString(idColIndex);
        view.setTag(holder);

        return view;
    }

    @Override
    public void bindView(View view, Context context, final Cursor cursor) {

        NoteHolder holder = (NoteHolder) view.getTag();
        final int position = cursor.getPosition();

        holder.nameView.setText(holder.title);
        holder.cognomView.setText(holder.cognom);
        holder.idView.setText(holder.id);

        holder.checkbox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                cursor.moveToPosition(position);
                Log.d("checkBoxState", Arrays.toString(checkBoxState));
                if (((CheckBox) v).isChecked()){
                    checkBoxState[cursor.getPosition()] = true;
                    Log.d("checkBoxState", "isChecked //cursor.getpositioin="+cursor.getPosition());
                }
                else {
                    checkBoxState[cursor.getPosition()] = false;
                    Log.d("checkBoxState", "is not Checked //cursor.getpositioin="+cursor.getPosition());
                }

            }
        });
        holder.checkbox.setChecked(checkBoxState[cursor.getPosition()]);

        //holder.priorityView.setImageResource(holder.priorityResId);
        //Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/ARLRDBD.TTF");
        //holder.nameView.setTypeface(tf);
        //holder.idView.setTypeface(tf);
    }

    private static class NoteHolder {
        TextView nameView;
        TextView cognomView;
        TextView idView;
        CheckBox checkbox;

        //ImageView priorityView;
        String title;
        String cognom;
        String id;
        int priorityResId;
    }
}